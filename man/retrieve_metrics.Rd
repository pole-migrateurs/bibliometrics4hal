% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/retrieve_metrics.R
\name{retrieve_metrics}
\alias{retrieve_metrics}
\title{Retrieve metrics}
\usage{
retrieve_metrics(html_page)
}
\arguments{
\item{html_page}{A html page as produced by \code{\link[=load_hal_html]{load_hal_html()}}}
}
\value{
A data.frame with the metrics
}
\description{
Retrieve information on the number of downloads and views
}
\examples{
load_hal_html(document_id = "hal-03866335") |>
  retrieve_metrics()
}
