---
title: "Development actions history"
output: html_document
editor_options:
  chunk_output_type: console
---

All commands that you use to use when developing packages...

# First time just after creating the project

- Fill the following chunk to create the DESCRIPTION of your package

```{r description}
# Describe your package
fusen::fill_description(
  pkg = here::here(),
  fields = list(
    Title = "Bibliometrics for HAL",
    Description = "[Hal science](<https://hal.science/>) is a French open archive for research institute. We develop a [R](https://cran.r-project.org/) package to gather and analyse statistics for document in that platform. Its was primarly designed to analyse publications from [Management of Diadromous Fish in their Environment, OFB, INRAE, Institut Agro, UNIV PAU & PAYS ADOUR/E2S UPPA portal](https://hal.science/POLE_MIGRATEURS_AMPHIHALINS) but can be used to analyse any other HAL portal.", # nolint: line_length_linter.
    `Authors@R` = c(
      person("Laurent", "Beaulaton", email = "laurent.beaulaton@ofb.gouv.fr", role = c("aut", "cre"), comment = c(ORCID = "0000-0002-9614-8602")) # nolint: line_length_linter.
    )
  ), overwrite = TRUE
)
```

We choose a [CeCILL-2 license](https://spdx.org/licenses/CECILL-2.1.html) compatible with [French administration rule](https://www.data.gouv.fr/fr/pages/legal/licences/) as well as with [GPL](https://www.gnu.org/licenses/license-list.en.html#CeCILL).

# Start using git

```{r, eval=FALSE}
# Deal with classical files to ignore
usethis::git_vaccinate()
```

# Set extra sources of documentation

```{r, eval=FALSE}
# README
usethis::use_readme_rmd()
# Code of Conduct
usethis::use_code_of_conduct("laurent.beaulaton [at] ofb.gouv.fr")
```

**From now, you will need to "inflate" your package at least once to be able to use the following commands. Let's go to your flat template, and come back here later if/when needed.**


# Package development tools
## Use once

```{r, eval=FALSE}
# package-level documentation
usethis::use_package_doc()

# _GitLab
gitlabr::use_gitlab_ci(type = "check-coverage-pkgdown")
```

## Use everytime needed

```{r, eval=FALSE}
# Simulate package installation
pkgload::load_all()

# Generate documentation and deal with dependencies
attachment::att_amend_desc()

# Check the package
devtools::check()
testthat::test_local()

# Add a new flat template
fusen::add_flat_template("add")
```

# Share the package

```{r, eval=FALSE}
# set and try pkgdown documentation website locally
usethis::use_pkgdown()
pkgdown::build_site()

# build the tar.gz with vignettes to share with others
devtools::build(vignettes = TRUE)
```
