
<!-- README.md is generated from README.Rmd. Please edit that file -->

# bibliometrics4hal

<!-- badges: start -->

[![Contributor
Covenant](https://img.shields.io/badge/Contributor%20Covenant-2.1-4baaaa.svg)](CODE_OF_CONDUCT.md)
<!-- badges: end -->

[Hal science](https://hal.science/) is a French open archive for
research institute. We develop this [R](https://cran.r-project.org/)
package to gather and analyse statistics for document in that platform.
Its was primarly designed to analyse publications from [Management of
Diadromous Fish in their Environment, OFB, INRAE, Institut Agro, UNIV
PAU & PAYS ADOUR/E2S UPPA
portal](https://hal.science/POLE_MIGRATEURS_AMPHIHALINS) but can be used
to analyse any other HAL portal.

This package was develop thanks to
[{fusen}](https://thinkr-open.github.io/fusen/) R package.

## Code of Conduct

Please note that the bibliometrics4hal project is released with a
[Contributor Code of Conduct](CODE_OF_CONDUCT.md) and its [French
Version](CODE_OF_CONDUCT_FR.md). By contributing to this project, you
agree to abide by its terms.

## Installation

You can install the development version of {bibliometrics4hal} like so:

``` r
remotes::install_git("https://forgemia.inra.fr/pole-migrateurs/bibliometrics4hal", ref = "main")
```

## Principle

We are interested by the number of Views and Downloads of a document in
HAL portal. It is on the HTML page but not reachable from [HAL
API](https://api.archives-ouvertes.fr/docs/search) (Confirmed by HAL
support the 08/11/2023).

<div class="figure">

<img src="man/figures/README-hal_example.png" alt="Number of Views and Downloads of a HAL document" width="100%" />
<p class="caption">
Number of Views and Downloads of a HAL document
</p>

</div>

These information are accessible from the HTML code.

<div class="figure">

<img src="man/figures/README-hal_example_code.png" alt="Number of Views and Downloads of a HAL document - HTML code" width="100%" />
<p class="caption">
Number of Views and Downloads of a HAL document - HTML code
</p>

</div>

## Example

In this example we just take one HAL pulication and retrive the number
of Views and Donwloads.

``` r
library(bibliometrics4hal)
## basic example code
html_page <- load_hal_html(document_id = "hal-03866335")
retrieve_metrics(html_page)
#>     Views Downloads
#>        20        16
```
